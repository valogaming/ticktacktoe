-- Global variables
p1Points = 0
p2Points = 0
activePlayer = 1

fontFamily = "fonts/ComputerSpeak.ttf"

font = 
{    
    titleFont  = love.graphics.newFont(fontFamily, 60),
    pointsFont = love.graphics.newFont(fontFamily, 100),
    textFont   = love.graphics.newFont(fontFamily, 24),
    playerFont = love.graphics.newFont(fontFamily, 30)
}

board = {}

tokens = 
{
    topL = "",
    topC = "",
    topR = "",
    midL = "",
    midC = "",
    midR = "",
    botL = "",
    botC = "",
    botR = ""
}

-- End Global variables

function love.load()
    if arg[#arg] == "-debug" then require("mobdebug").start() end
    
    for i = 1,3 do
        board[i] = {}
        for j=1,3 do
            board[i][j] = 0
        end
    end
end

function drawPlayer(mouseX, mouseY, activePlayer)
    -- recoger mousecoords
    -- comparar con posicion del tablero
    -- pintar ficha en función del activePlayer
    -- cambiar el activePlayer
    love.graphics.setFont(font.playerFont)
    love.graphics.setColor(255, 255, 255)
    
    if activePlayer == 1 then
        token = "O"
        playerToken = 1
    else
        token = "X"
        playerToken = 2
    end

    if mouseX >= 40 and mouseX <= 187 then
        if mouseY >= 100 and mouseY <= 257 then
            tokens.topL = token
            board[1][1] = playerToken
        elseif mouseY >= 286 and mouseY <= 432 then
            tokens.midL = token
            board[1][2] = playerToken
        elseif mouseY >= 460 and mouseY <= 619 then
            tokens.botL = token
            board[1][3] = playerToken
        end
    elseif mouseX >= 200 and mouseX <= 376 then
        if mouseY >= 103 and mouseY <= 260 then
            tokens.topC = token
            board[2][1] = playerToken
        elseif mouseY >=280 and mouseY <= 435 then
            tokens.midC = token
            board[2][2] = playerToken
        elseif mouseY >= 456 and mouseY <= 619 then
            tokens.botC = token
            board[2][3] = playerToken            
        end
    elseif mouseX >= 394 and mouseX <= 543 then
        -- TODO last board row
    end

    for i = 1,3 do
        for j=1,3 do
            print(board[i][j])
        end
    end

    
end

function drawBoard()
    -- Board draw
    love.graphics.setColor(13, 124, 91)
    local vertical1   = {186, 100, 206, 100, 206, 620, 186, 620}
    local vertical2   = {372, 100, 392, 100, 392, 620, 372, 620}
    local horizontal1 = {80, 256, 500, 256, 500, 276, 80, 276}
    local horizontal2 = {80, 432, 500, 432, 500, 452, 80, 452}

    love.graphics.polygon('fill', vertical1)
    love.graphics.polygon('fill', vertical2)
    love.graphics.polygon('fill', horizontal1)
    love.graphics.polygon('fill', horizontal2)

    
end

function drawBackground()
    love.graphics.setColor(55, 198, 155)
    love.graphics.polygon('fill', 0, 0, 640, 0, 640, 720, 0, 720)
    love.graphics.setColor(0, 0, 0)
    love.graphics.polygon('fill', 640, 0, 1280, 0, 1280, 720, 640, 720)
end

function drawPointCounter()
    love.graphics.setColor(255, 255, 255)
    love.graphics.setFont(font.titleFont)
    love.graphics.print("POINTS", 860, 100)
    love.graphics.setFont(font.pointsFont)
    love.graphics.print(p1Points, 840, 250)
    love.graphics.print(p2Points, 1040, 250)
end

function love.draw()
    drawBackground()
    drawPointCounter()
    drawBoard()

    love.graphics.setFont(font.textFont)
    love.graphics.print("- Press R to reload the game\n- Press Q to exit the game", 760, 520)

    love.graphics.setFont(font.titleFont)

    local boardX = 100
    local boardY = 100
    local counter = 1

    for k in pairs(tokens) do
        
        love.graphics.print(tokens[k], boardX, boardY)
-- TODO loop to optimize this flow
        --[[
        love.graphics.print(tokens.topC, 200, 100)
        love.graphics.print(tokens.topR, 300, 100)
        love.graphics.print(tokens.midL, 100, 300)
        love.graphics.print(tokens.midC, 200, 300)
        love.graphics.print(tokens.midR, 300, 300)
        love.graphics.print(tokens.botL, 100, 500)
        love.graphics.print(tokens.botC, 200, 500)
        love.graphics.print(tokens.botR, 300, 500) 
        ]]
    end
end

function love.update(dt)
    -- print(love.mouse.getX() .. " " .. love.mouse.getY())
    function love.mousepressed(x, y, button, istouch)
        if button == 1 then
            print("cursor X: " .. love.mouse.getX() .. "\ncursor Y: " .. love.mouse.getY())
            
            drawPlayer(love.mouse.getX(), love.mouse.getY(), activePlayer)
            
            if activePlayer == 1 then
                activePlayer = activePlayer + 1
            else
                activePlayer = activePlayer - 1
            end
        end
    end
    
    function love.keypressed(key)
        if key == 'r' then
            activePlayer = 1
            p1Points = 0
            p2Points = 0
            
            for k in pairs(tokens) do
                tokens[k] = ""
            end
        end

        if key == 'q' then
            love.event.quit()
        end
    end
end